package social;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import social.selenium.steps.GoogleSteps;

@RunWith(SerenityRunner.class)
public class GoogleTest {

    @Steps
    private
    GoogleSteps memberSteps;

    @Test
    public void membersShouldStartWithBronzeStatus() {
        memberSteps.open_landing_page();
    }
}

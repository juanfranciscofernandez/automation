package social;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import social.selenium.steps.GmailSteps;

@RunWith(SerenityRunner.class)
public class GmailTest {

    @Steps
    private
    GmailSteps memberSteps;

    @Test
    public void membersShouldStartWithBronzeStatus() {
        memberSteps.open_landing_page();
    }
}

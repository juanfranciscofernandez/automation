package social;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import social.selenium.steps.CrearCuentaGmailSteps;

/**
 * Created by Kiko2 on 29/07/2017.
 */
@RunWith(SerenityRunner.class)
public class CrearCuentaGmailTest {
    @Steps
    private
    CrearCuentaGmailSteps memberSteps;

    @Test
    public void membersShouldStartWithBronzeStatus() {
        memberSteps.create_account_google();
    }
}

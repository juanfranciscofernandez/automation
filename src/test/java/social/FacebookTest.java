package social;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import social.selenium.steps.CrearCuentaGmailSteps;

@RunWith(SerenityRunner.class)
public class FacebookTest {

    @Steps
    private
    CrearCuentaGmailSteps memberSteps;

    @Test
    public void membersShouldStartWithBronzeStatus() {
        memberSteps.create_account_google();
    }
}

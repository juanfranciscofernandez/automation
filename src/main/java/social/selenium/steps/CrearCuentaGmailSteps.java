package social.selenium.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Kiko2 on 29/07/2017.
 */
public class CrearCuentaGmailSteps extends ScenarioSteps
{

    public CrearCuentaGmailSteps(Pages pages) {
        super(pages);
    }

    @Step
    public CrearCuentaGmailSteps create_account_google() {
        Properties prop = new Properties();
        String  ENVIRONMENT = System.getProperty("someproperties").toString();
        InputStream input = null;
        try {

            input = new FileInputStream("src/main/resources/profiles/application-"+ENVIRONMENT+".properties");

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            System.out.println("Entorno");
            System.out.println(prop.getProperty("driverbinario"));
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Database Properties: ");
        System.setProperty("webdriver.gecko.driver",prop.getProperty("driverbinario"));
        WebDriver driver = new FirefoxDriver();
        driver.get("https://accounts.google.com/SignUp?continue=https%3A%2F%2Fwww.google.es%2F%3Fgws_rd%3Dssl&hl=es");
        driver.quit();
        return this;

    }
}

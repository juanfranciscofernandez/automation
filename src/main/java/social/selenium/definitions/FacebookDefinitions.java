package social.selenium.definitions;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import social.selenium.steps.FacebokSteps;

/**
 * Created by Kiko2 on 27/07/2017.
 */
public class FacebookDefinitions {

    @Steps
    FacebokSteps fb;

    @Given("the user accesses the facebook landing page")
    public void open_facebook_page(){
        fb.open_landing_page();
    }

}

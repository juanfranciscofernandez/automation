package social.selenium.definitions;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import social.selenium.steps.GmailSteps;

/**
 * Created by Kiko2 on 27/07/2017.
 */
public class GmailDefinitions {

    @Steps
    GmailSteps fb;

    @Given("the user accesses the gmail landing page")
    public void open_facebook_page(){
        fb.open_landing_page();
    }
}

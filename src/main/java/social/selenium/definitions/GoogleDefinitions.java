package social.selenium.definitions;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import social.selenium.steps.GoogleSteps;

/**
 * Created by marek5050 on 2/25/16.
 */
public class GoogleDefinitions {
    @Steps
    GoogleSteps gs;

    @Given("the user accesses the google landing page")
    public void open_landing_page(){
        gs.open_landing_page();
    }

}
